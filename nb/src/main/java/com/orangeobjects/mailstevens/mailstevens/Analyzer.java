/*
 *  O R A N G E   O B J E C T S
 *  copyright by Orange Objects
 * 
 *  http://www.OrangeObjects.de
 * 
 *  $Id$
 */
package com.orangeobjects.mailstevens.mailstevens;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.util.MimeMessageParser;

/**
 *
 * @author Michael Hofmann <Michael.Hofmann@OrangeObjects.de>
 */
public class Analyzer {

    static final Logger LOGGER = Logger.getLogger(Analyzer.class.getName());

    /*  ***********************************************************************
     *  C o n s t r u c t o r
     **************************************************************************/
    void process() throws FileNotFoundException, MessagingException, Exception {

        File file = new File("/usr/local/data/home/michael/tmp/tmp/", "74925.mail");
        assert file.exists();
        FileInputStream fis = new FileInputStream(file);
        try {
            MimeMessage emailMessage = new MimeMessage(null, fis);
            MimeMessageParser parser = new MimeMessageParser(emailMessage);
            parser = parser.parse();
            String content = null;
            if (parser.hasPlainContent()) {
                content = parser.getPlainContent();
            } else if (parser.hasHtmlContent()) {
//                Document doc = Jsoup.parse(parser.getHtmlContent());
                content = "HTML Content!";
            }
            System.out.println("mail subject          : {}" + parser.getSubject());
            System.out.println("mail has plain content: {}" + parser.hasPlainContent());
            System.out.println("mail has html content : {}" + parser.hasHtmlContent());
            System.out.println("mail has attachments  : {}" + parser.hasAttachments());
            System.out.println("mail plain content    : {}" + StringUtils.abbreviate(parser.getPlainContent(), 200));
            System.out.println("mail html content     : {}" + StringUtils.abbreviate(parser.getHtmlContent(), 200));

        } finally {
            if (fis != null) {
                fis.close();
            }
        }
    }


    /*  ***********************************************************************
     *  G e t t e r  und  S e t t e r
     **************************************************************************/
}
