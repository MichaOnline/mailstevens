/*
 *  O R A N G E   O B J E C T S
 *  copyright by Orange Objects
 * 
 *  http://www.OrangeObjects.de
 * 
 *  $Id$
 */

package com.orangeobjects.mailstevens.mailstevens;

import javax.mail.MessagingException;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Michael Hofmann <Michael.Hofmann@OrangeObjects.de>
 */
public class AnalyzerTest {

    public AnalyzerTest() {
    }

    /*  ***********************************************************************
     *  T e s t
     **************************************************************************/
    //
    @Test
    public void testDummy() throws Exception {
        
        Analyzer anal = new Analyzer();
        anal.process();

    }

    @Test
    public void testSomeMethod() {
    }
}